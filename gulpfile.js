var elixir = require('laravel-elixir');

require('laravel-elixir-css-url-adjuster');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix
        .urlAdjuster('resources/assets/bower/datatables.net-dt/css/jquery.dataTables.css', {
            replace: ['/images', '/img']
        }, 'resources/assets/bower/datatables.net-dt/css')
        .styles([
            'bower/datatables.net-bs/css/dataTables.bootstrap.css',
            'bower/bootstrap/dist/css/bootstrap.css',
            'bower/font-awesome/css/font-awesome.css',
            'bower/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
            //'bower/AdminLTE/dist/css/AdminLTE.css',
            //'bower/AdminLTE/dist/css/skins/skin-purple.css',
            'css/app.css'
        ], 'public/assets/css/app.css', 'resources/assets')
        .copy('resources/assets/bower/font-awesome/fonts', 'public/build/assets/fonts')
        .copy('resources/assets/bower/bootstrap/dist/fonts', 'public/build/assets/fonts')
        .copy('resources/assets/bower/datatables.net/images', 'public/build/assets/img')
        .scripts([
            'bower/jquery/dist/jquery.js',
            'bower/bootstrap/dist/js/bootstrap.js',
            'bower/datatables.net/js/jquery.dataTables.js',
            'bower/datatables.net-bs/js/dataTables.bootstrap.js',
            'bower/moment/min/moment.min.js',
            'bower/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            //'bower/AdminLTE/dist/js/app.js',
            'js/app.js'
        ], 'public/assets/js/app.js', 'resources/assets')
        .version(['assets/css/app.css', 'assets/js/app.js']);
});
