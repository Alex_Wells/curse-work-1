<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Carbon\Carbon;

$factory->define(App\Student::class, function (Faker\Generator $faker) {
    $birthday = $faker->dateTimeBetween('-17 years', '-6 years');

    return [
        'first_name' => $faker->firstName,
        'second_name' => $faker->lastName,
        'third_name' => 'Third',
        'email' => $faker->safeEmail,
        'birthday' => $birthday,
        'school_entry' => Carbon::instance($birthday)->addYears(6),
        'form' => chr(65 + rand(0,3)), // generates form 1-11 and letter in upper case in range of A-D
    ];
});
