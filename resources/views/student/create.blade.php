@extends('app')

@section('heading')
    Створення студента
@endsection

@section('content')
    <form class="form-horizontal" method="POST" action="/students">
        <input type="hidden" name="_method" value="POST">
        @include('student._form', ['attrs' => []])
    </form>
@endsection
