@extends('app')

@section('heading')
    Список студентів
@endsection

@section('content')
    <table class="table table-bordered table-hover dt-bootstrap" id="students" cellspacing="0" width="100%" data-order='[[1, "asc"]]'>
        <thead>
            <tr>
                <th data-sortable="false">Дії</th>
                <th>Ім'я</th>
                <th>Прізвище</th>
                <th>По батькові</th>
                <th>Клас</th>
                <th>Вік</th>
                <th>Дата народження</th>
                <th>Дата вступу</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            @forelse($students as $student)
                <tr>
                    <td data-student-id="{{ $student->id }}">
                        <button type="button" class="btn btn-xs btn-danger idDeleteStudent">
                            <span class="glyphicon glyphicon-trash"></span>&nbsp;
                        </button>
                        <button type="button" class="btn btn-xs btn-warning idEditStudent">
                            <span class="glyphicon glyphicon-edit"></span>&nbsp;
                        </button>
                    </td>
                    <td>{{ $student->first_name }}</td>
                    <td>{{ $student->second_name }}</td>
                    <td>{{ $student->third_name }}</td>
                    <td>{{ $student->form }}</td>
                    <td>{{ $student->age }}</td>
                    <td>{{ $student->birthday_clear }}</td>
                    <td>{{ $student->school_entry_clear }}</td>
                    <td>{{ $student->email }}</td>
                </tr>
            @empty
                <tr>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
