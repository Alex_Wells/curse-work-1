@extends('app')

@section('heading')
    Редагування студента #{{ $student->id }}
@endsection

@section('content')
    <form class="form-horizontal" method="POST" action="/students/{{ $student->id }}">
        <input type="hidden" name="_method" value="PUT">
        @include('student._form', ['attrs' => $student->getAttributes()])
    </form>
@endsection
