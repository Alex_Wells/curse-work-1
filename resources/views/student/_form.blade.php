<form class="form-horizontal">
    <div class="form-group">
        <label for="inputFirstName" class="col-sm-1 control-label">Ім'я</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="inputFirstName" placeholder="Ім'я" name="first_name" value="{{ $attrs['first_name'] or '' }}">
        </div>
    </div>
    <div class="form-group">
        <label for="inputSecondName" class="col-sm-1 control-label">Прізвище</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="inputSecondName" placeholder="Прізвище" name="second_name" value="{{ $attrs['second_name'] or '' }}">
        </div>
    </div>
    <div class="form-group">
        <label for="inputThirdName" class="col-sm-1 control-label">По батькові</label>
        <div class="col-sm-4">
            <input type="text" class="form-control" id="inputThirdName" placeholder="По батькові" name="third_name" value="{{ $attrs['third_name'] or '' }}">
        </div>
    </div>
    <div class="form-group">
        <label for="inputEmail" class="col-sm-1 control-label">Email</label>
        <div class="col-sm-4">
            <input type="email" class="form-control" id="inputEmail" placeholder="Email" name="email" value="{{ $attrs['email'] or '' }}">
        </div>
    </div>
    <div class="form-group">
        <label for="inputBirthday" class="col-sm-1 control-label">Дата народження</label>
        <div class="col-sm-4">
            <div class="input-group date">
                <input class="form-control idDatePicker" id="inputBirthday" placeholder="" name="birthday" data-date="{{ $attrs['birthday'] or '' }}">
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="inputSchoolEntry" class="col-sm-1 control-label">Дата вступу</label>
        <div class="col-sm-4">
            <div class="input-group date">
                <input class="form-control idDatePicker" id="inputSchoolEntry" placeholder="" name="school_entry" data-date="{{ $attrs['school_entry'] or \Carbon\Carbon::now()->format('Y-m-d') }}">
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="inputForm" class="col-sm-1 control-label">Клас</label>
        <div class="col-sm-4">
            <select class="form-control" id="inputForm">
                <option {{ (array_key_exists('form', $attrs) && $attrs['form'] == 'А') ? 'selected' : '' }}>А</option>
                <option {{ (array_key_exists('form', $attrs) && $attrs['form'] == 'Б') ? 'selected' : '' }}>Б</option>
                <option {{ (array_key_exists('form', $attrs) && $attrs['form'] == 'В') ? 'selected' : '' }}>В</option>
                <option {{ (array_key_exists('form', $attrs) && $attrs['form'] == 'Г') ? 'selected' : '' }}>Г</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-1 col-sm-10">
            <button type="submit" class="btn btn-success">Зберегти</button>
        </div>
    </div>
</form>