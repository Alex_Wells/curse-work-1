<ul class="sidebar-nav">
    <li class="sidebar-brand">
        <a href="/">
            База студентів
        </a>
    </li>
    @if(Auth::check())
        <li>
            <a href="{{ url('/students') }}">Список студентів</a>
        </li>
        <li>
            <a href="{{ url('/students/create') }}">Створити запис</a>
        </li>
        <li>
            Авторизовано як {{ Auth::user()->name }}
        </li>
        <li>
            <a href="{{ url('/logout') }}">Вийти</a>
        </li>
    @else
        <li><a href="{{ url('/login') }}">Авторизація</a></li>
        <li><a href="{{ url('/register') }}">Реєстрація</a></li>
    @endif
</ul>