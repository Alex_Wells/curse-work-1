<div class="modal fade" id="confirmDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Видалення запису #<span class="idConfirmDeleteId"></span>
            </div>
            <div class="modal-body">
                Ви впевнені, що хочете видалити запис про студента #<span class="idConfirmDeleteId"></span>?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Відмінити</button>
                <a class="btn btn-danger btn-ok" id="removeStudent" data-student-id="">Видалити</a>
            </div>
        </div>
    </div>
</div>