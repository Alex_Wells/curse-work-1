<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>База студентів</title>

        <link rel="stylesheet" href="{{ elixir('assets/css/app.css') }}">

        <style>
            .fa-btn {
                margin-right: 6px;
            }
        </style>
    </head>
    <body id="app-layout">

        <div id="sidebar-wrapper">
            @include('partials.sidebar')
        </div>

        <div id="wrapper">
            <div id="page-content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">@yield('heading')</div>
                                <div class="panel-body">
                                    @yield('content')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('partials.modals')

        <script src="{{ elixir('assets/js/app.js') }}"></script>
    </body>
</html>
