$(document).ready(function() {
    var path = window.location.pathname.substring(1).replace(new RegExp('[0-9]\/', 'g'), '');

    $('.idDatePicker').each(function() {
        $(this).datetimepicker({
            format: 'd-m-Y',
            defaultDate: $(this).attr('data-date')
        });
    });

    switch(path) {
        case 'students':
            initStudentsPage();
            break;
    }

    function initStudentsPage() {
        $('#students').DataTable({
            columnDefs: [
                { "width": "60px", "targets": 0 }
            ],
            bLengthChange: false,
            iDisplayLength: 20,
            language: {
                "sProcessing":   "Зачекайте...",
                "sLengthMenu":   "Показати _MENU_ записів",
                "sZeroRecords":  "Записи відсутні.",
                "sInfo":         "Записи з _START_ по _END_ із _TOTAL_ записів",
                "sInfoEmpty":    "Записи з 0 по 0 із 0 записів",
                "sInfoFiltered": "(відфільтровано з _MAX_ записів)",
                "sInfoPostFix":  "",
                "sSearch":       "Пошук:",
                "sUrl":          "",
                "oPaginate": {
                    "sFirst": "Перша",
                    "sPrevious": "Попередня",
                    "sNext": "Наступна",
                    "sLast": "Остання"
                },
                "oAria": {
                    "sSortAscending":  ": активувати для сортування стовпців за зростанням",
                    "sSortDescending": ": активувати для сортування стовпців за спаданням"
                }
            }
        });

        // show confirmation
        $('.idDeleteStudent').click(function() {
            var studentId = $(this).parent().attr('data-student-id');

            $('.idConfirmDeleteId').text(studentId);
            $('#removeStudent').attr('data-student-id', studentId);

            $('#confirmDelete').modal();
        });

        // remove the student
        $('#removeStudent').click(function() {
            var studentId = $(this).attr('data-student-id');

            post('DELETE', '/students/' + studentId);
        });

        $('.idEditStudent').click(function() {
            var studentId = $(this).parent().attr('data-student-id');

            window.location.href = '/students/' + studentId + '/edit';
        });
    }

    function post(method, path, parameters) {
        parameters = parameters || {};

        var form = $('<form></form>');

        form.attr('method', 'post');
        form.attr('action', path);

        parameters['_method'] = method;

        $.each(parameters, function(key, value) {
            var field = $('<input>');

            field.attr('type', 'hidden');
            field.attr('name', key);
            field.attr('value', value);

            form.append(field);
        });

        // The form needs to be a part of the document in
        // order for us to be able to submit it.
        $(document.body).append(form);
        form.submit();
    }

});