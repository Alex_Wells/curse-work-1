<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

    protected $fillable = ['first_name', 'second_name', 'third_name', 'email', 'birthday', 'school_entry', 'form'];

    protected $dates = ['birthday', 'school_entry'];

    public function getNameAttribute() {
        $names = array_only($this->attributes, ['first_name', 'second_name', 'third_name']);

        return implode(' ', $names);
    }

    public function getAgeAttribute() {
        return Carbon::now()->diffInYears($this->birthday);
    }

    public function getBirthdayClearAttribute() {
        return $this->birthday->format('d-m-Y');
    }

    public function getSchoolEntryClearAttribute() {
        return $this->school_entry->format('d-m-Y');
    }

    public function getFormAttribute($form_letter) {
        $now = Carbon::now();
        $form_year = $now->diffInYears($this->school_entry);

        if($now->month < 9) $form_year++;

        return $form_year . '-' . $form_letter;
    }

    public function setBirthdayAttribute($value) {
        $this->attributes['birthday'] = Carbon::createFromFormat('d-m-Y', $value);
    }

    public function setSchoolEntryAttribute($value) {
        $this->attributes['school_entry'] = Carbon::createFromFormat('d-m-Y', $value);
    }

}
